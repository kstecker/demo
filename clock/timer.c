#include "timer.h"

void InitializeQueue(Queue *q) {
	q -> num_elements = 0;
	q -> front = 0;
	q -> back = 0;
	q -> strings = malloc(COLS * ROWS * NUMS * sizeof(char *));
}

/* source: by geek for geeks*/
void Enqueue(Queue *q, char string[COLS]) {
	if (q -> num_elements + 1 >= QUEUE_SIZE) {
		exit(EXIT_FAILURE);
	}
	q -> strings[q -> back % QUEUE_SIZE] = string;
	q -> strings[q -> back] = string;
	q -> num_elements++;
	q -> back ++;
}

/* source: by geek for geeks*/
char* Dequeue(Queue *q) {
	if (q -> num_elements <= 0)
	{
		exit(EXIT_FAILURE);
	}
	if (q -> num_elements + 1 <= 0)
	{
		exit(EXIT_FAILURE);
	}
	char *value = q -> strings[q -> front % QUEUE_SIZE];
	q -> front ++;
	q -> num_elements--;
	return value;
}

//Initialize the clock data structure
void initClock( ClockType *clock ) {
	//original attempt at arrays
	int *num_arrays = (int *)malloc(NUMS * sizeof(int *));
	
	clock -> zero = (char **)malloc(ROWS * sizeof(char *));
	clock -> zero[0] = " @@@   ";
	clock -> zero[1] = "@   @  ";
	clock -> zero[2] = "@   @  ";
	clock -> zero[3] = "@   @  ";
	clock -> zero[4] = "@   @  ";
	clock -> zero[5] = "@   @  ";
	clock -> zero[6] = "@   @  ";
	clock -> zero[7] = " @@@   ";

	clock -> one = (char **)malloc(ROWS * sizeof(char *));
	clock -> one[0] = "  @    ";
	clock -> one[1] = " @@    ";
	clock -> one[2] = "  @    ";
	clock -> one[3] = "  @    ";
	clock -> one[4] = "  @    ";
	clock -> one[5] = "  @    ";
	clock -> one[6] = "  @    ";
	clock -> one[7] = "@@@@@  ";

	clock -> two = (char **)malloc((ROWS) * sizeof(char *));
	clock -> two[0] = " @@@   ";
	clock -> two[1] = "@   @  ";
	clock -> two[2] = "    @  ";
	clock -> two[3] = "    @  ";
	clock -> two[4] = "   @   ";
	clock -> two[5] = "  @    ";
	clock -> two[6] = " @     ";
	clock -> two[7] = "@@@@@  ";

	clock -> three = (char **)malloc((ROWS) * sizeof(char *));
	clock -> three[0] = " @@@   ";
	clock -> three[1] = "@   @  ";
	clock -> three[2] = "    @  ";
	clock -> three[3] = "  @@   ";
	clock -> three[4] = "    @  ";
	clock -> three[5] = "    @  ";
	clock -> three[6] = "@   @  ";
	clock -> three[7] = " @@@   ";

	clock -> four = (char **)malloc((ROWS) * sizeof(char *));
	clock -> four[0] = "   @   ";
	clock -> four[1] = "  @@   ";
	clock -> four[2] = " @ @   ";
	clock -> four[3] = "@@@@@  ";
	clock -> four[4] = "   @   ";
	clock -> four[5] = "   @   ";
	clock -> four[6] = "   @   ";
	clock -> four[7] = "   @   ";

	clock -> five = (char **)malloc((ROWS) * sizeof(char *));
	clock -> five[0] = "@@@@@  ";
	clock -> five[1] = "@      ";
	clock -> five[2] = "@      ";
	clock -> five[3] = "@@@@   ";
	clock -> five[4] = "    @  ";
	clock -> five[5] = "    @  ";
	clock -> five[6] = "@   @  ";
	clock -> five[7] = " @@@   ";

	clock -> six = (char **)malloc((ROWS) * sizeof(char *));
	clock -> six[0] = " @@@   ";
	clock -> six[1] = "@   @  ";
	clock -> six[2] = "@      ";
	clock -> six[3] = "@@@@   ";
	clock -> six[4] = "@   @  ";
	clock -> six[5] = "@   @  ";
	clock -> six[6] = "@   @  ";
	clock -> six[7] = " @@@   ";

	clock -> seven = (char **)malloc((ROWS) * sizeof(char *));
	clock -> seven[0] = "@@@@@  ";
	clock -> seven[1] = "    @  ";
	clock -> seven[2] = "    @  ";
	clock -> seven[3] = "    @  ";
	clock -> seven[4] = "    @  ";
	clock -> seven[5] = "    @  ";
	clock -> seven[6] = "    @  ";
	clock -> seven[7] = "    @  ";

	clock -> eight = (char **)malloc((ROWS) * sizeof(char *));
	clock -> eight[0] = " @@@   ";
	clock -> eight[1] = "@   @  ";
	clock -> eight[2] = "@   @  ";
	clock -> eight[3] = " @@@   ";
	clock -> eight[4] = "@   @  ";
	clock -> eight[5] = "@   @  ";
	clock -> eight[6] = "@   @  ";
	clock -> eight[7] = " @@@   ";

	clock -> nine = (char **)malloc((ROWS) * sizeof(char *));
	clock -> nine[0] = "@@@@@  ";
	clock -> nine[1] = "@   @  ";
	clock -> nine[2] = "@   @  ";
	clock -> nine[3] = "@@@@@  ";
	clock -> nine[4] = "    @  ";
	clock -> nine[5] = "    @  ";
	clock -> nine[6] = "    @  ";
	clock -> nine[7] = "    @  ";

	clock -> colon = (char **)malloc((ROWS) * sizeof(char *));
	clock -> colon[0] = "       ";
	clock -> colon[1] = "  @    ";
	clock -> colon[2] = "  @    ";
	clock -> colon[3] = "       ";
	clock -> colon[4] = "       ";
	clock -> colon[5] = "  @    ";
	clock -> colon[6] = "  @    ";
	clock -> colon[7] = "       ";
}

void printTimer(int* my_num, ClockType *clock) {
	Queue q;
	InitializeQueue(&q);

	//char *name;

	for (int i = 0; i < ROWS; i++) 
	{
		for (int j = 0; j < SIZE; j++) 
		{
		// 	char num = current_time[j];

			int num = my_num[j];

			if (num == 0) {
				Enqueue(&q, clock -> zero[i]);
			}
			else if (num == 1) {
				Enqueue(&q, clock -> one[i]);
			}
			else if (num == 2) {
				Enqueue(&q, clock -> two[i]);
			}
			else if (num == 3) {
				Enqueue(&q, clock -> three[i]);
			}
			else if (num == 4) {
				Enqueue(&q, clock -> four[i]);
			}
			else if (num == 5) {
				Enqueue(&q, clock -> five[i]);
			}
			else if (num == 6) {
				Enqueue(&q, clock -> six[i]);
			}
			else if (num == 7) {
				Enqueue(&q, clock -> seven[i]);
			}
			else if (num == 8) {
				Enqueue(&q, clock -> eight[i]);
			}
			else if (num == 9) {
				Enqueue(&q, clock -> nine[i]);
			}
			else Enqueue(&q, clock -> colon[i]);
		}
		for (int j = 0; j < SIZE; j++) 
		{
			//char *to_print = Dequeue(clock -> q);
			char *to_print = Dequeue(&q);
			printf("%s", to_print);
		}
		printf("\n");
	}
	sleep(1);

}

void initTimer(ClockType *clock, int minutes, int seconds) {

	int num[SIZE];

	for (int k = seconds; k >= 0; k--) {
    	int result = (int) minutes / 10;
    	num[0] = result;
    	num[1] = minutes % 10;
    	num[2] = ':';
    	int result2 = (int) seconds / 10;
    	num[3] = result2;
    	num[4] = seconds % 10;
    	//sleep(1);
    	//printf("num[0]: %d num[1]: %d num[2]: %c num[3]: %d num[4]:%d\n", num[0], num[1], num[2], num[3], num[4]);
    	printTimer(num, clock);
    	printf("\n");
    	seconds --;
    }
    //printf("%02d:%02d\n", minutes, seconds);
    if (minutes >= 1) 
    {	
     	minutes --;
	    for (int i = minutes; i >= 0; --i) 
	    {
	    	for (int j = 59; j > 0; --j) 
	    	{
	    		int result = (int) minutes / 10;	
		    	num[0] = result;
		    	num[1] = minutes % 10;
		    	num[2] = ':';
		    	int result2 = (int) j / 10;
		    	num[3] = result2;
		    	num[4] = j % 10;
		    	//sleep(1);
		    	//printf("num[0]: %d num[1]: %d num[2]: %c num[3]: %d num[4]:%d\n", num[0], num[1], num[2], num[3], num[4]);
		    	printTimer(num, clock);
		    	printf("\n");
	    		//printf("%02d:%02d\n", minutes, j);
	    	}
    		int result = (int) minutes / 10;	
	    	num[0] = result;
	    	num[1] = minutes % 10;
	    	num[2] = ':';
	    	int result2 = (int) (seconds + 1) / 10;
	    	num[3] = result2;
	    	num[4] = (seconds + 1) % 10;
	    	//sleep(1);
	    	//printf("num[0]: %d num[1]: %d num[2]: %c num[3]: %d num[4]:%d\n", num[0], num[1], num[2], num[3], num[4]);
	    	printTimer(num, clock);
	    	printf("\n");
	    	//printf("%02d:%02d\n", minutes, (seconds+1));
	    	minutes --;
	    }
	}

}

void runTimer() {
	int minutes, seconds, total;
	int check = 1;

    printf("How long should the timer run (MM:SS)?");
    scanf("%02d:%02d", &minutes, &seconds);
    //printf("Entered time is: %02d:%02d\n", minutes, seconds);

    total = (minutes * 60) + seconds;
    //printf("Total time in seconds is %d\n", total);

    ClockType clock;
	initClock(&clock);

    initTimer(&clock, minutes, seconds);

	cleanTimer(&clock);
}

void cleanTimer(ClockType  *clock) {
		free(clock -> zero);
		free(clock -> one);
		free(clock -> two);
		free(clock -> three);
		free(clock -> four);
		free(clock -> five);
		free(clock -> seven);
		free(clock -> eight);
		free(clock -> nine);
}