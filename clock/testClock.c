// #include <stdio.h>
// #include <stdlib.h>
// #include <time.h>
// #include <string.h>
#include "clock.h"

int main() {

	ClockType clock;

	initClock(&clock);

	//Print an ASCII clock showing cur_time as the time
	time_t cur_time = time(NULL);

	printClock(cur_time, &clock);

	// Free up any dynamically allocated memory in the clock
	cleanClock(&clock);

}