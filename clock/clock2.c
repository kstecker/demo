#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "clock.h"

//moved to clock.h
// typedef struct {
// 	//TODO: Define your ACII clock data structure here.
// 	int **number_arrays[NUMS];
// 	char *zero[ROWS];
// 	char *one[ROWS];
// 	char *two[ROWS];
// 	char *three[ROWS];
// 	char *four[ROWS];
// 	char *five[ROWS];
// 	char *six[ROWS];
// 	char *seven[ROWS];
// 	char *eight[ROWS];
// 	char *nine[ROWS];

// }ClockType;

// typedef struct 
// {
// 	int num_elements;
// 	char *strings[QUEUE_SIZE];
// 	int front;
// 	int back;
// }Queue;

void InitializeQueue(Queue *q) {
	q -> num_elements = 0;
	q -> front = 0;
	q -> back = 0;
	//q -> strings = (char *)malloc(COLS * ROWS * NUMS * sizeof(char *));
	q -> strings = malloc(COLS * ROWS * NUMS * sizeof(char *));
}

/* source: by geek for geeks*/
void Enqueue(Queue *q, char string[COLS]) {
	if (q -> num_elements + 1 >= QUEUE_SIZE) {
		exit(EXIT_FAILURE);
	}
	printf("Queueing: %s\n", string);
	printf("\tThe index for the front of the queue to start is %d\n", q->front);
    printf("\tThe index for the back of the queue to start is %d\n", q->back);
    printf("\tThe num_elements to start with is: %d\n", q->num_elements);
	//q -> strings[q -> back % QUEUE_SIZE] = string;
	//q -> strings[q -> back] = string;
	strcpy(q -> strings[q -> back % QUEUE_SIZE], string);
	q -> num_elements++;
	q -> back ++;
	printf("\tThe index for the front of the queue AT END of Enq is %d\n", q->front);
    printf("\tThe index for the back of the queue AT END of Enq is %d\n", q->back);
    printf("\tThe num_elements to end with is: %d\n", q->num_elements);
    char *value2 = q -> strings[q -> front % QUEUE_SIZE];
    printf("Next item set for dequeueing is: %s\n", value2);
}

/* source: by geek for geeks*/
char* Dequeue(Queue *q) {
	if (q -> num_elements <= 0)
	{
		exit(EXIT_FAILURE);
	}
	if (q -> num_elements + 1 <= 0)
	{
		exit(EXIT_FAILURE);
	}
	 printf("\tThe index for the front of the queue to start is %d\n", q->front);
     printf("\tThe index for the back of the queue to start is %d\n", q->back);
     printf("\tThe num_elements to start with is: %d\n", q->num_elements);
	char *value = q -> strings[q -> front % QUEUE_SIZE];
	//char *value = q -> strings[q -> front];
	q -> front ++;
	q -> num_elements--;
	printf("Dequeueing: %s\n", value);
	printf("\tThe index for the front of the queue AT END is %d\n", q->front);
    printf("\tThe index for the back of the queue AT END is %d\n", q->back);
    printf("\tThe num_elements to end with is: %d\n", q->num_elements);
    char *value2 = q -> strings[q -> front % QUEUE_SIZE];
    printf("Next item set for dequeueing is: %s\n", value2);
	return value;
}

/**********************************************************
 * Returns 1 (True) if someone is in the front of the queue
 * Returns 0 (False) if no one is in the front of the queue
 * ********************************************************/
char *Front (Queue *q){
    if (q -> num_elements == 0) {
	// if (q -> front == q -> back) {
		return NULL;
	}
	else {
		return q -> strings[q -> front%QUEUE_SIZE];
	}
	
}

//Initialize the clock data structure
void initClock( ClockType *clock ) {
	//original attempt at arrays
	int *num_arrays = (int *)malloc(NUMS * sizeof(int *));

	for (int i = 0; i < NUMS; i++) 
	{
		//num_arrays[i] = i;
		//clock->number_arrays[i] = &num_arrays[i];
		//printf("num_arrays[i] = %d\n", num_arrays[i]);
		//printf("Clock.num_arrays[i] = %d\n", *clock->number_arrays[i]);
	}

	//second attempt at num_arrays
	//char **number_arrays[NUMS] = {clock -> *zero, clock -> *two, clock -> *three, clock -> *four, clock -> *five, clock -> *six, clock -> *seven, clock -> *eight, clock -> *nine};

	clock -> zero = (char *)malloc((ROWS) * sizeof(char *));
	for (int i = 0; i < ROWS; ++i)
	{
		//clock -> zero = (char **)malloc((COLS) * sizeof(char *));
		clock -> zero[i] = malloc((COLS) * sizeof(char *));
	}

	clock -> zero[0] = " @@@ ";
	clock -> zero[1] = "@   @";
	clock -> zero[2] = "@   @";
	clock -> zero[3] = "@   @";
	clock -> zero[4] = "@   @";
	clock -> zero[5] = "@   @";
	clock -> zero[6] = "@   @";
	clock -> zero[7] = " @@@ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> one[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> one[0] = "  @  ";
	clock -> one[1] = " @@  ";
	clock -> one[2] = "  @  ";
	clock -> one[3] = "  @  ";
	clock -> one[4] = "  @  ";
	clock -> one[5] = "  @  ";
	clock -> one[6] = "  @  ";
	clock -> one[7] = " @@@ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> two[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> two[0] = " @@@ ";
	clock -> two[1] = "@   @";
	clock -> two[2] = "    @";
	clock -> two[3] = "    @";
	clock -> two[4] = "   @ ";
	clock -> two[5] = "  @  ";
	clock -> two[6] = " @   ";
	clock -> two[7] = "@@@@@";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> three[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> three[0] = " @@@ ";
	clock -> three[1] = "@   @";
	clock -> three[2] = "    @";
	clock -> three[3] = "  @@ ";
	clock -> three[4] = "    @";
	clock -> three[5] = "    @";
	clock -> three[6] = "@   @";
	clock -> three[7] = " @@@ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> four[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> four[0] = "   @ ";
	clock -> four[1] = "  @@ ";
	clock -> four[2] = " @ @ ";
	clock -> four[3] = "@@@@@";
	clock -> four[4] = "   @ ";
	clock -> four[5] = "   @ ";
	clock -> four[6] = "   @ ";
	clock -> four[7] = "   @ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> five[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> five[0] = "@@@@@";
	clock -> five[1] = "@    ";
	clock -> five[2] = "@    ";
	clock -> five[3] = "@@@@ ";
	clock -> five[4] = "    @";
	clock -> five[5] = "    @";
	clock -> five[6] = "@   @";
	clock -> five[7] = " @@@ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> six[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> six[0] = " @@@ ";
	clock -> six[1] = "@   @";
	clock -> six[2] = "@    ";
	clock -> six[3] = "@@@@ ";
	clock -> six[4] = "@   @";
	clock -> six[5] = "@   @";
	clock -> six[6] = "@   @";
	clock -> six[7] = " @@@ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> seven[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> seven[0] = "@@@@@";
	clock -> seven[1] = "    @";
	clock -> seven[2] = "    @";
	clock -> seven[3] = "    @";
	clock -> seven[4] = "    @";
	clock -> seven[5] = "    @";
	clock -> seven[6] = "    @";
	clock -> seven[7] = "    @";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> eight[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> eight[0] = " @@@ ";
	clock -> eight[1] = "@   @";
	clock -> eight[2] = "@   @";
	clock -> eight[3] = " @@@ ";
	clock -> eight[4] = "@   @";
	clock -> eight[5] = "@   @";
	clock -> eight[6] = "@   @";
	clock -> eight[7] = " @@@ ";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> nine[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> nine[0] = "@@@@@";
	clock -> nine[1] = "@   @";
	clock -> nine[2] = "@   @";
	clock -> nine[3] = "@@@@@";
	clock -> nine[4] = "    @";
	clock -> nine[5] = "    @";
	clock -> nine[6] = "    @";
	clock -> nine[7] = "    @";

	for (int i = 0; i < ROWS; ++i)
	{
		clock -> colon[i] = (char *)malloc(COLS * sizeof(char *));
	}

	clock -> colon[0] = "     ";
	clock -> colon[1] = "  @  ";
	clock -> colon[2] = "  @  ";
	clock -> colon[3] = "     ";
	clock -> colon[4] = "     ";
	clock -> colon[5] = "  @  ";
	clock -> colon[6] = "  @  ";
	clock -> colon[7] = "     ";

	//test: moving next 3 lines to printClock
	// Queue q;
	// InitializeQueue(&q);

	// char* queue_array = (char *)malloc(COLS * ROWS * NUMS * sizeof(char *));

	// for (int i = 0; i < NUMS; i++) 
	// {
	// 	queue_array[i] = i;
	// 	q.strings[i] = &queue_array[i];
	// 	//printf("num_arrays[i] = %d\n", num_arrays[i]);
	// 	//printf("Clock.num_arrays[i] = %d\n", *clock->number_arrays[i]);
	// }

	//q.num_elements = queue_array;
	//not sure what the below line is doing ...
	//clock -> q = &q;
}

//Print an ASCII clock showing cur_time as the time
void printClock( const time_t cur_time, const ClockType *clock ) {

	char *current_time;
	current_time = ctime(&cur_time);
	printf("%s", current_time);

	Queue q;
	InitializeQueue(&q);

	//char* queue_array = (char *)malloc(COLS * ROWS * NUMS * sizeof(char *));

	char *name;

	for (int i = 0; i < ROWS; i++) {
		for (int j = 11; j < 19; j++) {
			//name = switch_num(current_time[j]);
			//printf("j is: %d, current_time is: %c, and name is: %s\n", j, current_time[j], name);
			//Enqueue(&clock -> q, &name[i]);
			char num = current_time[j];
			//printf("num = %c\n", num);
			if (num == '0') {
				Enqueue(&q, clock -> zero[i]);
			}
			else if (num == '1') {
				Enqueue(&q, clock -> one[i]);
			}
			else if (num == '2') {
				Enqueue(&q, clock -> two[i]);
			}
			else if (num == '3') {
				Enqueue(&q, clock -> three[i]);
			}
			else if (num == '4') {
				Enqueue(&q, clock -> four[i]);
			}
			else if (num == '5') {
				Enqueue(&q, clock -> five[i]);
			}
			else if (num == '6') {
				Enqueue(&q, clock -> six[i]);
			}
			else if (num == '7') {
				Enqueue(&q, clock -> seven[i]);
			}
			else if (num == '8') {
				Enqueue(&q, clock -> eight[i]);
			}
			else if (num == '9') {
				Enqueue(&q, clock -> nine[i]);
			}
			else Enqueue(&q, clock -> colon[i]);
		}
		for (int j = 11; j < 19; j++) {
			char *to_print = Dequeue(clock -> q);
			printf("%s", to_print);
		}
		printf("\n");

		}
	}

	//This is working code! It prints the time vertically
	// for (int i = 11; i < 19; i++)
	// {	
	// 	// printf("%c/n", current_time[i]);
	// 	if (current_time[i] == '0')
	// 	{	
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> zero[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '1')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> one[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '2')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> two[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '3')
	// 	{
	// 			for (int i = 0; i < (ROWS); i++)
	// 			{
	// 				printf("%s\n", clock -> three[i]);
	// 			}
	// 	}
	// 	else if (current_time[i] == '4')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> four[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '5')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> five[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '6')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> six[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '7')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> seven[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '8')
	// 	{
	// 		for (int i = 0; i < (ROWS); i++)
	// 		{
	// 			printf("%s\n", clock -> eight[i]);
	// 		}
	// 	}
	// 	else if (current_time[i] == '9')
	// 	{
	// 			for (int i = 0; i < (ROWS); i++)
	// 			{
	// 				printf("%s\n", clock -> nine[i]);
	// 			}
	// 	}
	// }

//}

// Free up any dynamically allocated memory in the clock
void cleanClock( ClockType *clock ) {

	free(clock -> number_arrays);

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> zero[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> one[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> two[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> three[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> four[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> five[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> six[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> seven[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> eight[i]);
	}

	for (int i = 0; i < ROWS; ++i)
	{
		free(clock -> nine[i]);
	}
}



// int main() {

// 	ClockType *clock;

// 	//Print an ASCII clock showing cur_time as the time
// 	//Commenting out below line sunday afternoon,rest was already commented out
// 	//time_t cur_time;
	
// 	//void printClock( const time_t cur_time, const ClockType *clock );

// 	// Free up any dynamically allocated memory in the clock
// 	//void cleanClock( ClockType *clock);

// 	//char *number_arrays = (char *)malloc(NUMS * sizeof(char *));

// 	// for (int i = 0; i < NUMS; i++) 
// 	// {
// 	// 	num_arrays[i] = i;
// 	// 	clock->num_arrays[i] = &num_arrays[i];
// 	// 	printf("num_arrays[i] = %d\n", num_arrays[i]);
// 	// 	printf("Clock.num_arrays[i] = %d\n", *clock->num_arrays[i]);
// 	// }

// 	int count = 0;

// 	//char *test_one = (char *)malloc(ROWS * COLS * sizeof(char *));
// 	//char *one[ROWS];




// 	// char **number_arrays[NUMS] = {zero, two, three, four, five, six, seven, eight, nine};
// 	// **GETTING SEG FAULT WITH THIS ARRAY
// 	//for (int i = 0; i < NUMS; i++)
// 	// {
// 	// 	printf("Testing number_arrays[%d]: %c\n",i, **number_arrays[i]);
// 	// }

// 	//int *testone[ROWS];



// 	// for (int i = 0; i < ROWS; i++) 
// 	// {
// 	// 	for (int j = 0; j < COLS; j++)
// 	// 	{	
// 	// 		one[i][j] = count;
// 	// 		count += 1;
// 	// 		printf(" %d\n", one[i][j]);
// 	// 	}
// 	// }



// 	time_t now;

// 	//needed to get current time
// 	time(&now);

// 	// char current_time[26];

// 	// for (int i = 0; i < 26; i++)
// 	// {
// 	// 	current_time[i] = ctime()
// 	// }

// 	//below was working but
// 	//moving it to print struct? 
// 	//feels wrong but don't know how
// 	//else to match function sig
// 	// char *current_time;
// 	// current_time = ctime(&now);
// 	// printf("%s", current_time);

// 	Queue q;
// 	InitializeQueue(&q);

// 	ClockType my_clock;
// 	initClock(&my_clock);
// 	printClock(now, &my_clock);
// 	//cleanClock(&my_clock);

// 	int time[8];

// 	//testing queue
// 	// for (int i = 11; i < 19; i++)
// 	// {
// 		//Enqueue(&q, &current_time[i]);


// 	//new code below here from Sunday afternoon

// 	// char *to_print1;
// 	// char *to_print2;
// 	// char *to_print3;
// 	// char *to_print4;
// 	// char *to_print5;
// 	// char *to_print6;
// 	// char *to_print7;
// 	// char *to_print8;


// 	// Enqueue(&q, my_clock.zero[0]);
// 	// Enqueue(&q, my_clock.zero[1]);
// 	// Enqueue(&q, my_clock.zero[2]);
// 	// Enqueue(&q, my_clock.zero[3]);
// 	// Enqueue(&q, my_clock.zero[4]);
// 	// Enqueue(&q, my_clock.zero[5]);
// 	// Enqueue(&q, my_clock.zero[6]);
// 	// Enqueue(&q, my_clock.zero[7]);
	
// 	// to_print1 = Dequeue(&q);
// 	// to_print2 = Dequeue(&q);
// 	// to_print3 = Dequeue(&q);
// 	// to_print4 = Dequeue(&q);
// 	// to_print5 = Dequeue(&q);
// 	// to_print6 = Dequeue(&q);
// 	// to_print7 = Dequeue(&q);
// 	// to_print8 = Dequeue(&q);

// 	// printf("%s\n", to_print1);
// 	// printf("%s\n", to_print2);
// 	// printf("%s\n", to_print3);
// 	// printf("%s\n", to_print4);
// 	// printf("%s\n", to_print5);
// 	// printf("%s\n", to_print6);
// 	// printf("%s\n", to_print7);
// 	// printf("%s\n", to_print8);




// 	// for (int i = 0; i < ROWS; i++)
// 	// {

// 	// 	Enqueue(&q, my_clock.zero[i]);
// 	// 	Enqueue(&q, my_clock.one[i]);
// 	// 	Enqueue(&q, my_clock.two[i]);
// 	// 	Enqueue(&q, my_clock.three[i]);
// 	// 	Enqueue(&q, my_clock.four[i]);
// 	// 	Enqueue(&q, my_clock.five[i]);
// 	// 	Enqueue(&q, my_clock.six[i]);
// 	// }	

// 	// //}


// 	// for (int i = 0; i < ROWS; i++)
// 	// {
// 	// 	*to_print1 = *Dequeue(&q);
// 	// 	*to_print2 = *Dequeue(&q);
// 	// 	*to_print3 = *Dequeue(&q);
// 	// 	*to_print4 = *Dequeue(&q);
// 	// 	*to_print5 = *Dequeue(&q);
// 	// 	*to_print6 = *Dequeue(&q);

// 	// 	printf("%s", to_print1);
// 	// 	printf("%s", to_print2);
// 	// 	printf("%s", to_print3);
// 	// 	printf("%s", to_print4);
// 	// 	printf("%s", to_print5);
// 	// 	printf("%s", to_print6);

// 	// }

	
// 	return 0;
// }