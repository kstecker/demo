#ifndef CLOCK_H_
#define CLOCK_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define NUMS 11
#define ROWS 8
#define COLS 8
#define QUEUE_SIZE 400

typedef struct 
{
	int num_elements;
	char **strings;
	int front;
	int back;
}Queue;

typedef struct {
	//old: int *num_arrays[NUMS];
	//int *number_arrays[NUMS];
	char **zero;
	char **one;
	char **two;
	char **three;
	char **four;
	char **five;
	char **six;
	char **seven;
	char **eight;
	char **nine;
	char **colon;
	int timer_amount;

	Queue* q;

}ClockType;

//Initialize the clock data structure
void initClock( ClockType *clock );

//Print an ASCII clock showing cur_time as the time
void printClock( const time_t cur_time, const ClockType *clock );

// Free up any dynamically allocated memory in the clock
void cleanClock( ClockType *clock);

#endif /* CLOCK_H_ */