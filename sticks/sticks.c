#include <stdio.h>
#include <stdlib.h>
#include <time.h>      
#include <string.h>

int getUserChoice() {
    /* TODO: Prompt the user "Player 1: How many sticks do you take (1-3)?" and return
       an integer. Check that the value is valid (1, 2, or 3) and print an error if 
       is not, then ask again until a valid number is entered. You can exit the program with
       exit(1);
     */
	int check = 1;
	int taken;

	while (check != 0) {
		printf("How many sticks do you want to take (1-3)?\n");
		scanf("%d", &taken);
		if (taken < 1 || taken > 3) {
			printf("Error. Please try again.\n");
		}
		else {
			check = 0;
			//return taken;
		}
	}
	return taken;
}

int getComputerChoice(int current_sticks) {

    /* get a pseudo-random integer between 1 and 3 (inclusive) */
    int rand_choice = rand() % 3 + 1;

    if (rand_choice > current_sticks) return current_sticks;

    //if (current_sticks == 4) return 3;
    if (current_sticks % 4 + 1 == 1) return 3;

    //if (current_sticks == 3) return 2;
	if (current_sticks % 4 + 1 == 3) return 1;

    if (current_sticks % 4 + 1 == 4) return 2;

    // if (current_sticks == 6) return 1;

    // if (current_sticks == 7) return 2;

    // if (current_sticks == 8) return 3;
    /* Optionally replace with additional logic for the computer's strategy */

    return rand_choice;
}


int main(int argc, char** argv) 
{
    int user, computer, number_sticks;
    int check = 1;


    srand (time(NULL)); /* for reproducible results, you can call srand(1); */

    printf("Welcome to the game of sticks!\n");

    while (check != 0) {
	    printf("How many sticks are there on the table initially (10-100)? ");
	    scanf("%d", &number_sticks);

	    if (number_sticks < 8 || number_sticks > 100) {
	    	printf("Please enter a number between 10-100 or enter 0 to exit the program\n");
	    	// printf("How many sticks are there on the table initially (10-100)? ");
	    	// scanf("%d", &number_sticks);
	    }

	    else if (number_sticks == 0) {
	    	exit(EXIT_FAILURE);
	    }

	    else {
	    	check = 0;
	    }

    }
    
  
    while (number_sticks > 0) {
      printf("There are %d sticks on the board.\n", number_sticks); 
    	user = getUserChoice();
    	//printf("User takes %d\n", user);


    	if (user >= number_sticks) {
    		printf("You lose.\n");
    		return 0;
    	}

    	number_sticks -= user;
    	computer = getComputerChoice(number_sticks);
    	printf("Computer chooses %d.\n", computer);

    	if (computer >= number_sticks) {
    		printf("You win!\n");
    		return 0;
    	}

    	number_sticks -= computer;
    }



    /* TODO: check that num_sticks is between 10 and 100 (inclusive) and print 
                    an error and exit, if it is not.*/

    /* TODO: Main game loop:
      While some sticks remain:
        1. Human: Get number of sticks by calling getUserChoice
        2. Computer: get number of sticks by calling getComputerChoice
        3. Output the computer's choice, e.g., "Computer selects 3."
        3. Update number_sticks
        4. Print the current number of sticks, e.g., "There are 2 sticks on the board."
      After all the sticks are gone, output the result of the game, 
      e.g., "Computer wins." or "Computer loses."
     */

    return 0;
}

