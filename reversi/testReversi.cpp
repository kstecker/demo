#include "reversi.h"
#include <iostream>
#include <cstdlib> // Stream declarations
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <cstddef>  // For size_t
#define USEDEBUG

#ifdef USEDEBUG
#define DEBUG(X) cout << #X << ": " << (X) << endl;
#else
#define DEBUG(X)
#endif

using namespace std;


int main(int argc, char const *argv[])
{
	// BoardType myboard;
	GameType game;

	game.runGame();
	return 0;
}