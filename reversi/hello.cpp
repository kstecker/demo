#include <iostream>
#include <cstdlib> // Stream declarations
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <cstddef>  // For size_t
//define PR(x) cout << #x " = " << x << "\n";
#define P(A) cout << #A << ": " << (A) << endl;
using namespace std;

void replaceChars(string& modifyMe,
  const string& findMe, const string& newChars) {
  // Look in modifyMe for the "find string"
  // starting at position 0:
  size_t i = modifyMe.find(findMe, 0);
  // Did we find the string to replace?
  if(i != string::npos)
    // Replace the find string with newChars:
    modifyMe.replace(i, findMe.size(), newChars);
}

int main() {

	//catch an exception for string out of range handling
	string mystr("1234");
	// at() saves you by throwing an exception:
	try {
	mystr.at(5);
	} catch(exception& e) {
	cerr << e.what() << endl;
	}

	//way to insert strings into strings, calls above function
	string bigNews = "I thought I saw Elvis in a UFO. "
	           "I have been working too hard.";
	string replacement("wig");
	string findMe("UFO");
	// Find "UFO" in bigNews and overwrite it:
	replaceChars(bigNews, findMe, replacement);
	assert(bigNews == "I thought I saw Elvis in a "
	 "wig. I have been working too hard.");

	/* Inserting a char* into a string */
	string s("A piece of text");
	cout << s << endl;
	string tag("$tag$");
	s.insert(8, tag + ' ');
	cout << s << endl;
	assert(s == "A piece $tag$ of text");
	int start = s.find(tag);
	assert(start == 8);
	assert(tag.size() == 5);
	s.replace(start, tag.size(), "hello there");
	assert(s == "A piece hello there of text");
	cout << s << endl;


	/* String sizing example */
	string bigNews("I saw Elvis in a UFO. ");
	cout << bigNews << endl;
	// How much data have we actually got?
	cout << "Size = " << bigNews.size() << endl;
	// How much can we store without reallocating?
	cout << "Capacity = " << bigNews.capacity() << endl;
	// Insert this string in bigNews immediately
	// before bigNews[1]:
	bigNews.insert(1, " thought I");
	cout << bigNews << endl;
	cout << "Size = " << bigNews.size() << endl;
	cout << "Capacity = " << bigNews.capacity() << endl;
	// Make sure that there will be this much space
	bigNews.reserve(500);
	// Add this to the end of the string:
	bigNews.append("I've been working too hard.");
	cout << bigNews << endl;
	cout << "Size = " << bigNews.size() << endl;
	cout << "Capacity = " << bigNews.capacity() << endl;


	/* This is a great switch example */
  bool quit = false;  // Flag for quitting
  while(quit == false) {
    cout << "Select a, b, c or q to quit: ";
    char response;
    cin >> response;
    switch(response) {
      case 'a' : cout << "you chose 'a'" << endl;
                 break;
      case 'b' : cout << "you chose 'b'" << endl;
                 break;
      case 'c' : cout << "you chose 'c'" << endl;
                 break;
      case 'q' : cout << "quitting menu" << endl;
                 quit = true;
                 break;
      default  : cout << "Please use a,b,c or q!"
                 << endl;
    }
	int a = 1, b = 2, c = 3;
	P(a); P(b); P(c);
	P(a + b);
	P((c - a)/b);
  }


	// cout << "Hello, World! I am "
	// 	<< 8 << " Today " << endl;

	// system("Hello");

	// // Specifying formats with manipulators:
	// cout << "a number in decimal: "
	//    << dec << 15 << endl;
	// cout << "in octal: " << oct << 15 << endl;
	// cout << "in hex: " << hex << 15 << endl;
	// cout << "a floating-point number: "
	//    << 3.14159 << endl;
	// cout << "non-printing char (escape): "
	//    << char(27) << endl;

	//  cout << "This is far too long to put on a "
	// "single line but it can be broken up with "
	// "no ill effects\nas long as there is no "
	// "punctuation separating adjacent character "
	// "arrays.\n";

	int number;
	// cout << "Enter a decimal number: ";
	// cin >> number;
	// cout << "value in octal = 0:"
	// 	<< oct << number << endl;
	// cout << "value in hex = 0x" 
	// 	<< hex << number << endl;

	// string s1, s2; // Empty strings
	// string s3 = "Hello, World."; // Initialized
	// string s4("I am"); // Also initialized
	// s2 = "Today"; // Assigning to a string
	// s1 = s3 + " " + s4; // Combining strings
	// s1 += " 8 "; // Appending to a string
	// cout << s1 + s2 + "!" << endl;

	// //adds line by line
	// ifstream in("Scopy.cpp"); //open for reading
	// ofstream out("Scopy.cpp"); //open for writing
	// string s;
	// while (getline(in, s)) //Discards newline char
	// 	out << ss << "\n"; //have to add it back in

	// //copies whole file into one string arguement
	// ifstream in("FillString.cpp");
	// string s, line;
	// while(getline(in, line))
	// 	s += line + "\n";
	// cout << s;

	// vector<string> v;
	// ifstream in("Fillvector.cpp");
	// string line;
	// while(getline(in, line))
	// 	v.push_back(line); // Add the line to the end
	// // Add line numbers:
	// for(int i = 0; i < v.size(); i++)
	// 	cout << i << ": " << v[i] << endl;

}

// int main() {
//   cout << "Hello, World! I am "
//        << 8 << " Today!" << endl;
// }