#include "reversi.h"
#include <iostream>
#include <cstdlib> // Stream declarations
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <cstddef>  // For size_t
#define USEDEBUG

#ifdef USEDEBUG
#define DEBUG(X) cerr <<"Debugging "<< #X << ": " << (X) << endl;
#else
#define DEBUG(X)
#endif

using namespace std;

//notes to self: debug is off

int BoardType::logic(int color) {
	int check = 0;

	check += downLogic(color);
	check += upLogic(color);
	check += leftLogic(color);
	check += rightLogic(color);
	check += rd_diagonal(color);
	check += lu_diagonal(color);
	check += ld_diagonal(color);
	//above are working
	check += ru_diagonal(color);

	return check;
}

int BoardType::ru_diagonal(int color) {
	int check = 0;
	int count = 0;
	int row_spot = 0;
	int col_spot = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	//cout << "In ru_diagonal" << endl;
	// DEBUG(r_ptr)
	// DEBUG(c_ptr)
	// DEBUG(grid[r_ptr][c_ptr])
	if (row_spot > 0) {
		if (r_ptr > 0 && c_ptr > 0) {
			if (grid[r_ptr + 1][c_ptr - 1] == opponent) {
				for (int i = r_ptr + 1; i < size; i++) {
			        for (int j = c_ptr - 1; j > 0; j --) {
			        	// DEBUG(size)
			        	// DEBUG(i)
			        	// DEBUG(j)
			        	// DEBUG(grid[i][j])
						if (grid[i][j] == self){
							row_spot = i;
							col_spot = j;
							check += 1;
				        }
			       	}
			    }
			}
		}
	}
	// DEBUG(row_spot)
	// DEBUG(col_spot)
	//make sure it found a matching color
	if (row_spot > 0) {
		grid[r_ptr + 1][c_ptr - 1] = self;
		for (int i = (r_ptr); i < (row_spot); i++) {
			for (int j = (c_ptr - 1); j < (col_spot + 1); j--) {
				//make sure it's an opponent and it matches diagonal pattern
				if (grid[i][j] == opponent && j + i == size + 1) {
					// DEBUG(size)
		   //      	DEBUG(i)
		   //      	DEBUG(j)
		   //      	DEBUG(grid[i][j])
					grid[i][j] = self;
				}
			}
		}
	}
	return check;
}

int BoardType::ld_diagonal(int color) {
	int check = 0;
	int count = 0;
	int row_spot = 0;
	int col_spot = 0;
	int r_temp = 0;
	int c_temp = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	if (r_ptr - 1 < size) {
		if (grid[r_ptr - 1][c_ptr + 1] == opponent) {
			for (int i = r_ptr - 1; i > 0; i--) {
		        for (int j = c_ptr + 2; j < size; j ++) {
		        	// DEBUG(size)
		        	// DEBUG(i)
		        	// DEBUG(j)
		        	// DEBUG(grid[i][j])
					if (grid[i][j] == self){
						// cerr << "Found a match in up left diag" << endl;
						// DEBUG(grid[i][j])
						row_spot = i;
						col_spot = j;
						check += 1;
						// DEBUG(row_spot)
						// DEBUG(col_spot)
			        }
		       	}
		    }
		}
	}
	if (row_spot > 0) {
		grid[r_ptr - 1][c_ptr + 1] = self;
		for (int i = (r_ptr - 1); i > 0; i--) {
			for (int j = (c_ptr + 1); j < size; j++) {
				//make sure it's an opponent and it matches diagonal pattern
				if (grid[i][j] == opponent && j + i == size) {
					// DEBUG(size)
		   //      	DEBUG(i)
		   //      	DEBUG(j)
		   //      	DEBUG(grid[i][j])
					grid[i][j] = self;
				}
			}
		}
	}
	return check;

}

int BoardType::lu_diagonal(int color) {
	int check = 0;
	int count = 0;
	int row_spot = 0;
	int col_spot = 0;
	int r_temp = 0;
	int c_temp = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}
	//cout << "in lu_diag" << endl;
	// DEBUG(r_ptr + 1)
	// DEBUG(c_ptr + 1)
	if (r_ptr + 1 < size) {
		if (grid[r_ptr + 1][c_ptr + 1] == opponent) {
			for (int i = r_ptr + 1; i < size; i++) {
		        for (int j = c_ptr + 2; j < size + 1; j ++) {
						if (grid[i][j] == self){
							row_spot = i;
							col_spot = j;
							check += 1;
							// DEBUG(row_spot)
							// DEBUG(col_spot)
				        }
		       	}
		    }
		}
	}
	if (row_spot > 0) {
		// DEBUG(row_spot)
		// DEBUG(col_spot)
		grid[r_ptr + 1][c_ptr + 1] = self;
		for (int i = (r_ptr + 1); i < (row_spot + 1); i++) {
			for (int j = (c_ptr + 1); j < (col_spot + 1); j++) {
				//make sure it's an opponent and it matches diagonal pattern
				//if (grid[i][j] == opponent && j == i + 1) {
				if (grid[i][j] == opponent && j == i) {
					// DEBUG(size)
		   //      	DEBUG(i)
		   //      	DEBUG(j)
		   //      	DEBUG(grid[i][j])
					grid[i][j] = self;
				}
			}
		}
	}
	return check;

}

int BoardType::rd_diagonal(int color) {
	int check = 0;
	int count = 0;
	int row_spot = 0;
	int col_spot = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	//cout << "in rd_diag" << endl;
	//make sure on board
	if (r_ptr - 1 > 0 && c_ptr > 0) {
		//make sure near opponent
		if (grid[r_ptr - 1][c_ptr - 1] == opponent) {
			//make sure next space is not empty
			if (r_ptr - 2 > 0 && c_ptr -1 > 0) {  
				if (grid[r_ptr - 2][c_ptr - 2] != 0) {
					for (int i = r_ptr - 1; i > 0; i--) {
				        for (int j = c_ptr - 1; j > 0; j --) {
								if (grid[i][j] == self){
									// DEBUG(size)
						   //      	DEBUG(i)
						   //      	DEBUG(j)
						   //      	DEBUG(grid[i][j])
									row_spot = i;
									col_spot = j;
									check += 1;
									//DEBUG(row_spot)
									//DEBUG(col_spot)
						        }
				       	}
				    }
				}
			}
		}
	}

	// DEBUG(row_spot)
	// DEBUG(col_spot)
	//make sure it found a matching color
	if (row_spot > 0) {
		grid[r_ptr - 1][c_ptr - 1] = self;
		for (int i = (r_ptr - 1); i > row_spot; i--) {
			for (int j = (c_ptr - 1); j > col_spot; j--) {
				//make sure it's an opponent and it matches diagonal pattern
				//cout << "In ru_diagonal's change if statement";
			    // DEBUG(size)
	        	// DEBUG(i)
	        	// DEBUG(j)
	        	// DEBUG(grid[i][j])
				if (grid[i][j] == opponent && j == i) {
				//if (grid[i][j] == opponent) {
					grid[i][j] = self;
				}
			}
		}
	}
	return check;
}

int BoardType::rightLogic(int color) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	//cout << "In rightLogic" << endl;
	//DEBUG(r_ptr)
	//DEBUG(c_ptr)
	//DEBUG(grid[r_ptr][c_ptr])
	//DEBUG(opponent)
	//DEBUG(self)

	int row_spot = 0;
	if (grid[r_ptr][c_ptr + 1] == opponent) {
		for (int i = (c_ptr + 2); i < (size -1); i++) {
			if (grid[r_ptr][i] == self){
				//if friendly piece found, save spot and increment check
				row_spot = i;
				check += 1;
			}
		}
	}
	//flip all pieces between input and friendly piece
	if (row_spot > 0) {
		for (int i = (c_ptr + 1); i < (row_spot + 1); i++) {
			grid[r_ptr][i] = self;
		}
	}
	return check;
}

int BoardType::leftLogic(int color) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	//cout << "In leftLogic" << endl;
	//DEBUG(r_ptr)
	//DEBUG(c_ptr)
	//DEBUG(grid[r_ptr][c_ptr])
	//DEBUG(opponent)
	//DEBUG(self)

	int row_spot = 0;
	if (grid[r_ptr][c_ptr - 1] == opponent) {
		for (int i = (c_ptr - 2); i > 1; i--) {
			if (grid[r_ptr][i] == self){
				//if friendly piece found, save spot and increment check
				row_spot = i;
				check += 1;
			}
		}
	}
	// flip all pieces between input and friendly piece
	if (row_spot > 0) {
		for (int i = (c_ptr - 1); i >= (row_spot + 1); i--) {
			grid[r_ptr][i] = self;
		}
	}

	return check;
}

int BoardType::upLogic(int color) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	//cout << "In upLogic";
	//DEBUG(r_ptr)
	//DEBUG(c_ptr)
	//DEBUG(grid[r_ptr][c_ptr])

	//DEBUG(opponent)
	//DEBUG(self)

	int row_spot = 0;
	if (grid[r_ptr - 1][c_ptr] == opponent) {
		for (int i = (r_ptr - 2); i > 1; i--) {
			if (grid[i][c_ptr] == self){
				//when we find friendly piece, increment check and save spot
				row_spot = i;
				check += 1;
			}
		}
	}

	// flip all pieces between friendly spot and input
	if (row_spot > 0) {
		for (int i = (r_ptr - 1); i >= (row_spot + 1); i--) {
			if (grid[i][c_ptr] == empty) {
				i = row_spot + 1;
			}
			else {
				grid[i][c_ptr] = self;
			}
		}
	}
	return check;
}

int BoardType::downLogic(int color) {
	int check = 0;
	int count = 0;

	//cout << "Got into downLogic" << endl;
	//DEBUG(r_ptr)
	//DEBUG(c_ptr)
	//DEBUG(grid[r_ptr][c_ptr])
	//DEBUG(size)

	piece opponent;
	piece self;

	if (color == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}
	
	int row_spot = 0;
	//don't check off grid or seg fault
	if (r_ptr + 1 < size) {
		//make sure near opponent
		if (grid[r_ptr + 1][c_ptr] == opponent) {
			for (int i = (r_ptr + 2); i < (size -1); i++) {
				if (grid[i][c_ptr] == self){
					//save spot of friendly piece, and then increment check
					row_spot = i;
					check += 1;
				}
			}
		}
	}

	//change all pieces in between input and friendly piece
	if (row_spot > 0) {
		for (int i = (r_ptr + 1); i < (row_spot + 1); i++) {
			if (grid[i][c_ptr] == empty) {
				i = row_spot + 1;
			}
			else {
			grid[i][c_ptr] = self;
			}
		}
	}
	return check;
}


int BoardType::checkEmpty() {
	int check = 0;
	//cout << "In checkEmpty";
	//DEBUG(r_ptr)
	//DEBUG(c_ptr)
	//DEBUG(grid[r_ptr+ 1][c_ptr + 1])

	if (grid[r_ptr][c_ptr] == empty) {
		check = 1;
	}
	//DEBUG(grid[r_ptr][c_ptr])
	//DEBUG(check)
	return check;
}

void PlayerType::initPlayer(piece init_color) {
	color = init_color;
}

piece PlayerType::getColor() {
	return color;
}

int BoardType::getSize() {
	return size;

}

//Fill this in
// void BoardType::getMove(int r_ptr, int c_ptr) {
// 	return 0;
// }

void BoardType::cleanBoard() {
	for (int i = 0; i < size; i++) {
		delete []grid[i];
	}
	delete grid;
}

void BoardType::printBoard(){
	for (int i =0; i < size; i++)
	{
    	for (int j = 0; j < size; j++) 
    	{
    		if ((i == 0) && (j >= 1)) 
    		{
    			if (j <= 10)
    			{
    		 		printf(" %d ", j-1);
    			}
    			else
    			{
    				printf(" %d", j-1);
    			}
    		}
    		else if ((j == 0) && (i >= 1)) 
    		{
    			if (i <= 10)
    			{
    				printf(" %d ", i-1);
    			}
    			else
    			{
    				printf("%d ", i-1);
    			}
    		}
    		else if (grid[i][j] == empty) 
    		{
    			printf(" . ");
    		}
    		else if (grid[i][j] == black) 
    		{
    			printf(" X ");
    		}
    		else if (grid[i][j] == white) 
    		{
    			printf(" O ");
    		}
    		else 
    		{	
        		printf("   ");
        	}
    	}
    	printf("\n");
	}
}

void BoardType::move(PlayerType current_player) {
	// TODO: FIX INPUT... NOT WORKING !!!!
	//string input_str;
	// int r_ptr;
	// int c_ptr;
	int color = current_player.getColor();
	int check = 0;
	int error = false;
	int valid = 1;
	//int size = board -> size;
	char str[9];
	//char space;

		while (!error) {
			error = true;
			cin.clear();
			cin.ignore();
			cout << "Player " << color << " please enter a board position in format 0 0: ";
			cin >> r_ptr >> c_ptr;
			if (cin.fail()) {
				cout << "Incorrect input. Please enter a board position in format 0 0: ";
				//reset stream
				cin.clear();
				//ignore anything leftover
				cin.ignore();
				error = false;
			}

			//handling forfeiting a turn
			if (r_ptr == 100 && c_ptr == 100) {
				break;
			}

			if (r_ptr == 500 && c_ptr == 500) {
				break;
			}

			r_ptr += 1;
			c_ptr += 1;

			//make sure move is not off of the board
			if (r_ptr < 0 || r_ptr >= (size) || c_ptr < 0 || c_ptr >= (size)) {
				cout << "To forfeit your turn, type: 100 100. To end game type: 500 500" << endl;
	    		cout << "That space is not on the board" << endl;
	    		error = false;
			}

			else if (checkEmpty() == 0) {
				cout << "To forfeit your turn, type: 100 100. To end game type: 500 500" << endl;
	    		cout << "That space is not empty, please try again." << endl;
	    		error = false;
			}

			else if (logic(color) == 0) {
	    		cout << "To forfeit your turn, type: 100 100. To end game type: 500 500" << endl;
	    		cout << "That space is not valid, please try again." << endl;
	    		error = false;
	    	}

			// else if (logic(r_ptr + 1), (c_ptr + 1) == 0) {

			// }


			cin.clear();
		}

	// r_ptr += 1;
	// c_ptr += 1;

	// DEBUG(r_ptr);
	// DEBUG(c_ptr);

	if (r_ptr >= 100 && r_ptr < 500) {
		cout << "Player "<< color << " forfeits their turn.\n" << endl;
	}
	else if (r_ptr >= 500) {
		cout << "Ending game. Calculating results...\n" << endl;
		//get out of while loop
		moves_left = 0;
	}
	else {
		if (color == 1) {
			cout << "Making a move for black" << endl;
			// DEBUG(r_ptr)
			// DEBUG(c_ptr)
	    	grid[r_ptr][c_ptr] = black;
	    	moves_left -= 1;
	    	// DEBUG(grid[r_ptr][c_ptr])
		}
		else if (color == 2) {
			cout << "Making a move for white" << endl;
			// DEBUG(r_ptr)
			// DEBUG(c_ptr)
			grid[r_ptr][c_ptr] = white;
	    	// DEBUG(grid[r_ptr][c_ptr])
			moves_left -= 1;
		}
	}
	// printf("inputing %s into grid\n", board.grid[r_ptr][c_ptr]);

    printBoard();


}

void BoardType::initBoard() {
	int input_size, p1move, p2move, r_ptr, c_ptr, total_moves;
	int check = 0;
	int count = 0;
	char error;

	//BoardType board;

	//set up board
	//TODO MAKE SURE INPUT IS WITHIN RANGE
	while (size < 8 || size > 20) { 
	cout << "Please enter an even board size (8 - 20): " << endl;
	cin.clear();
	cin >> size;
	//DEBUG(size);

		if (cin.fail()) {
				cout << "Incorrect input. Please enter a board position in format 0 0: " << endl;
				//reset stream
				cin.clear();
				//ignore anything leftover
				cin.ignore();
		}
	}

	//FIX INVALID INPUT
	// if (size < 8 || size > 20) 
	// {
	//     	cout << "Please enter a board size between 8-20" << endl;
	//     	cin >> (size);
	// }

	moves_left = size * size;
	//DEBUG (moves_left);

	size += 1;

	grid = new piece *[size];
	for (int i = 0; i < size; i++) {
		grid[i] = new piece[size];
		for (int j = 0; j < size; j++) {
			if ((i == 0) && (j >= 1))
			{
				grid[i][j] = header;
			}
			else if ((j == 0) && (i >= 1))
			{
				grid[i][j] = header;
			}
			else {
				grid[i][j] = empty;
			}
		}
	}

	//set up initial pieces
    int midpoint = (size + 1) / 2;
    grid[midpoint][midpoint] =  black;
    grid[midpoint][midpoint-1] =  white;
    grid[midpoint-1][midpoint-1] =  black;
    grid[midpoint-1][midpoint] =  white;

    printBoard();
}


void GameType::runGame()
{
	int size, move, r_ptr, c_ptr;
	int check = 1;
	int b_total = 0;
	int w_total = 0;
	char error;

	// DEBUG(b_total);
	// DEBUG(w_total);

	player1.initPlayer(black);
	player2.initPlayer(white);
	//DEBUG(player1.color)
	//DEBUG(player2.color)

	//BoardType board;

	board.initBoard();

	while (board.moves_left > 0) {
		board.move(player1);
		board.moves_left -= 1;

		board.move(player2);
		board.moves_left -= 1;

    // while (moves_left != 0) {
    // 	move(player1);
    // 	move(player2);
    // }
	}

	size = board.getSize();

	for (int k = 0; k < size; k++) {
		for (int l = 0; l < size; l++) {
			if (board.grid[k][l] == black) {
				b_total += 1;
			}
			else if (board.grid[k][l] == white) {
				w_total += 1;
			}
		}
	}

	cout << "Total black pieces " << b_total << endl;
	cout << "Total white pieces " << w_total << endl;
	if (b_total > w_total) {
		cout << "Player 1 wins!" << endl;
	}
	else if (w_total > b_total) {
		cout << "Player 2 wins!" << endl;
	}
    else {
    	cout << "It's a tie?! Both players win!" << endl;
    }

    board.cleanBoard();
}



