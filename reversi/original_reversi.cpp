/* By Kristine Stecker 
   Worked on some white board logic with Bethany
   Just a head's up that I wanted you to know
   that I know that my rows and cols are off
   a bit because I used header pieces to print
   out my rows and columns in an orderly fashion */


#include "reversi.h"

#include <iostream>
using namespace std;
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cunistd>

int checkEmpty(BoardType *board, int r_ptr, int c_ptr){
	int check = 0;

	if (board -> grid[r_ptr][c_ptr] == empty)
		check = 1;
	return check;
}

int downLogic(BoardType *board, int player, int r_ptr, int c_ptr ) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (player == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}
	
	int row_spot = 0;
	//don't check off grid or seg fault
	if (r_ptr + 1 < board -> size) {
		//make sure near opponent
		if (board -> grid[r_ptr + 1][c_ptr] == opponent) {
			for (int i = (r_ptr + 2); i < (board->size -1); i++) {
				if (board -> grid[i][c_ptr] == self){
					//save spot of friendly piece, and then increment check
					row_spot = i;
					check += 1;
				}
			}
		}
	}

	//change all pieces in between input and friendly piece
	if (row_spot > 0) {
		for (int i = (r_ptr + 1); i < (row_spot + 1); i++) {
			board -> grid[i][c_ptr] = self;
		}
	}
	return check;
}

int upLogic(BoardType *board, int player, int r_ptr, int c_ptr) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (player == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	int row_spot = 0;
	if (board -> grid[r_ptr - 1][c_ptr] == opponent) {
		for (int i = (r_ptr - 2); i > 1; i--) {
			if (board -> grid[i][c_ptr] == self){
				//when we find friendly piece, increment check and save spot
				row_spot = i;
				check += 1;
			}
		}
	}

	// flip all pieces between friendly spot and input
	if (row_spot > 0) {
		for (int i = (r_ptr - 1); i >= (row_spot + 1); i--) {
			board -> grid[i][c_ptr] = self;
		}
	}
	return check;
}

int leftLogic(BoardType *board, int player, int r_ptr, int c_ptr) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (player == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	int row_spot = 0;
	if (board -> grid[r_ptr][c_ptr - 1] == opponent) {
		for (int i = (c_ptr - 2); i > 1; i--) {
			if (board -> grid[r_ptr][i] == self){
				//if friendly piece found, save spot and increment check
				row_spot = i;
				check += 1;
			}
		}
	}
	// flip all pieces between input and friendly piece
	if (row_spot > 0) {
		for (int i = (c_ptr - 1); i >= (row_spot + 1); i--) {
			board -> grid[r_ptr][i] = self;
		}
	}

	return check;
}

int rightLogic(BoardType *board, int player, int r_ptr, int c_ptr) {
	int check = 0;
	int count = 0;
	piece opponent;
	piece self;

	if (player == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	int row_spot = 0;
	if (board -> grid[r_ptr][c_ptr + 1] == opponent) {
		for (int i = (c_ptr + 2); i < (board->size -1); i++) {
			if (board -> grid[r_ptr][i] == self){
				//if friendly piece found, save spot and increment check
				row_spot = i;
				check += 1;
			}
		}
	}
	//flip all pieces between input and friendly piece
	if (row_spot > 0) {
		for (int i = (c_ptr + 1); i < (row_spot + 1); i++) {
			board -> grid[r_ptr][i] = self;
		}
	}
	return check;
}

int ld_diagonal(BoardType *board, int player, int r_ptr, int c_ptr){
	int check = 0;
	int count = 0;
	int row_spot = 0;
	int col_spot = 0;
	int r_temp = 0;
	int c_temp = 0;
	piece opponent;
	piece self;

	if (player == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}

	//printf("r_ptr is: %d, and c_ptr is: %d\n", r_ptr, c_ptr);
	//printf("player color is: %d, and opponent is: %d\n", player, opponent);
	//printf("r_ptr + 1 is: %d, and c_ptr + 1 is: %d\n", r_ptr + 1, c_ptr +1 );
	if (r_ptr + 1 < board -> size) {
		if (board -> grid[r_ptr + 1][c_ptr + 1] == opponent) {
			//printf("GOT HERE!\n");
			for (int i = r_ptr + 1; i < board -> size; i++) {
		        for (int j = c_ptr + 2; j < board -> size + 1; j ++) {
		            //if (i == j) {
				//printf("<<<MADE IT TO #2>>>\n");
						if (board -> grid[i][j] == self){
							//printf("<<<MADE IT TO #3>>>\n");
							//printf("row_spot is: %d\n", row_spot);
							row_spot = i;
							col_spot = j;
							check += 1;
				        }
		       	}
		    }
		}
	}
	//printf("Row_spot is:%d. Col_spot is %d\n", row_spot, col_spot);
	//make sure it found a matching color
	if (row_spot > 0) {
		board -> grid[r_ptr + 1][c_ptr + 1] = player;
		for (int i = (r_ptr + 1); i < (row_spot + 1); i++) {
			for (int j = (c_ptr + 1); j < (col_spot + 1); j++) {
				//make sure it's an opponent and it matches diagonal pattern
				if (board -> grid[i][j] == opponent && j == i + 1) {
					//if () {
					board -> grid[i][j] = player;
					//}
				}
			}
		}
	}
	return check;
}

int rd_diagonal(BoardType *board, int player, int r_ptr, int c_ptr){
	int check = 0;
	int count = 0;
	int row_spot = 0;
	int col_spot = 0;
	piece opponent;
	piece self;

	if (player == 1) {
		opponent = white;
		self = black;
	}
	else {
		opponent = black;
		self = white;
	}
	// printf("<<< In rd_diagonal >>>\n");
	// printf("r_ptr is: %d, and c_ptr is: %d\n", r_ptr, c_ptr);
	// printf("player color is: %d, and opponent is: %d\n", player, opponent);
	// printf("r_ptr - 1 is: %d, and c_ptr - 1 is: %d\n", r_ptr - 1, c_ptr - 1 );
	if (r_ptr - 1 > 0 && c_ptr > 0) {
	//printf("Looking at grid[r_ptr - 1][c_ptr - 1]: %d\n", board -> grid[r_ptr - 1][c_ptr - 1]);
		if (board -> grid[r_ptr - 1][c_ptr - 1] == opponent) {
			//printf("GOT HERE!\n");
			for (int i = r_ptr - 2; i > 0; i--) {
		        for (int j = c_ptr - 2; j > 0; j --) {
		            //if (i == j) {
					//printf("<<<MADE IT TO #2>>>\n");
						if (board -> grid[i][j] == self){
							//printf("<<<MADE IT TO #3>>>\n");
							//printf("row_spot is: %d\n", row_spot);
							row_spot = i;
							col_spot = j;
							check += 1;
				        }
		       	}
		    }
		}
	}
	//make sure it found a matching color
	if (row_spot > 0) {
		board -> grid[r_ptr - 1][c_ptr - 1] = player;
		for (int i = (r_ptr - 1); i < (row_spot + 1); i++) {
			for (int j = (c_ptr - 1); j < (col_spot + 1); j++) {
				//make sure it's an opponent and it matches diagonal pattern
				if (board -> grid[i][j] == opponent && j == i - 1) {
					//if () {
					board -> grid[i][j] = player;
					//}
				}
			}
		}
	}
	return check;
}	

int logic(BoardType *board, int player, int r_ptr, int c_ptr) {
	/* logic driver function */

	int check = 0;

	check += downLogic(board, player, r_ptr, c_ptr);
	check += upLogic(board, player, r_ptr, c_ptr);
	check += leftLogic(board, player, r_ptr, c_ptr);
	check += rightLogic(board, player, r_ptr, c_ptr);
	check += rd_diagonal(board, player, r_ptr, c_ptr);
	check += ld_diagonal(board, player, r_ptr, c_ptr);

	return check;
}

void cleanBoard(BoardType  *board) {
	int size = board -> size;

    for (int i =0; i < size; i++){
    	free(board->grid[i]);
    }
	free(board -> grid);   
}

void printBoard(BoardType *board){
	for (int i =0; i < board -> size; i++)
	{
    	for (int j = 0; j < board -> size; j++) 
    	{
    		if ((i == 0) && (j >= 1)) 
    		{
    			if (j <= 10)
    			{
    		 		printf(" %d ", j-1);
    			}
    			else
    			{
    				printf(" %d", j-1);
    			}
    		}
    		else if ((j == 0) && (i >= 1)) 
    		{
    			if (i <= 10)
    			{
    				printf(" %d ", i-1);
    			}
    			else
    			{
    				printf("%d ", i-1);
    			}
    		}
    		else if (board -> grid[i][j] == empty) 
    		{
    			printf(" . ");
    		}
    		else if (board -> grid[i][j] == black) 
    		{
    			printf(" X ");
    		}
    		else if (board -> grid[i][j] == white) 
    		{
    			printf(" O ");
    		}
    		else 
    		{	
        		printf("   ");
        	}
    	}
    	printf("\n");
	}
}

void clearInput() {
	/* found on class solution for project 1 
	   Consumes invalid input. */
	char c;
	while( ((c = getchar()) != EOF) && (c != '\n') );
	if (c == 'f'){
		exit(EXIT_FAILURE);
	}
	printf("Invalid input, please try again.\n");
	printf("To forfeit your turn, type: 100 100\n");
	printf("To end game, both players need to type: 500 500\n");
}

void move (BoardType *board, int player) {
	int move;
	int r_ptr, c_ptr;
	int check = 0;
	int error = 0;
	int valid = 1;
	int size = board -> size;
	char str[9];

	while (check != 1) 
	{
	    printf("Player %d please enter a board position in format 0 0: ", player);
	    error = scanf("%d %d", &r_ptr, &c_ptr);

	    while (error == 0){
	    	clearInput();
	    	printf("Player %d please enter a board position in format 0 0: ", player);
	    	error = scanf("%d %d", &r_ptr, &c_ptr);
	    } 
	    //printf("r_ptr is: %d, and c_ptr is: %d\n", r_ptr, c_ptr);
	    // r_ptr = move[0] - '0';
	    // c_ptr = move[1] - '0';

	    //if user enters input to forfeit their turn
	    if (r_ptr == 100 && c_ptr == 100) {
	    	break;
	    }

	    //if user enters input to end game early
	    if (r_ptr == 500 && c_ptr == 500) {

	    	break;
	    }
	    //if user inputs numbers off grid
	    if (r_ptr < 0 || r_ptr >= (size - 1) || c_ptr < 0 || c_ptr >= (size - 1)) {
	    	printf("To forfeit your turn, type: 100 100. To end game type: 500 500\n");
	    	printf("That space is not on the board\n");
	    }
	    else if (checkEmpty(board, (r_ptr + 1), (c_ptr + 1)) == 0) {
	    	printf("To forfeit your turn, type: 100 100. To end game type: 500 500\n");
	    	printf("That space is not empty, please try again.\n");
	    }

	    //if user picks incorrect spot
	    else if (logic(board, player, (r_ptr + 1), (c_ptr + 1)) == 0) {
	    	printf("To forfeit your turn, type: 100 100. To end game type: 500 500\n");
	    	printf("That space is not valid, please try again.\n");
	    }

	    // else if (move == 0) {
	    // 	exit(EXIT_FAILURE);
	    // }

	    else {
	    	check = 1;
	    }

    }

    r_ptr += 1;
	c_ptr += 1;

	if (r_ptr > 100 && r_ptr < 500) {
		printf("Player %d forfeits their turn.\n", player);
	}
	else if (r_ptr > 500) {
		printf("Ending game. Calculating results...\n");
		//get out of while loop
		board -> moves_left = 0;
	}
	else {
		if (player == 1) {
	    	board->grid[r_ptr][c_ptr] = black;
	    	board -> moves_left -= 1;
		}
		else if (player == 2) {
			board->grid[r_ptr][c_ptr] = white;
			board -> moves_left -= 1;
		}
	}
	// printf("inputing %s into grid\n", board.grid[r_ptr][c_ptr]);

    printBoard(board);
}

BoardType initBoard() {
	int size, p1move, p2move, r_ptr, c_ptr, total_moves;
	int check = 0;
	int count = 0;
	char error;

	//get board size input from user
	while (check != 1) 
	{
	    printf("Please enter an even board size (8-20)? ");
	    error = scanf("%d", &size);

	    while (error == 0){
	    	clearInput();
	    	printf("Please enter a number for a correct board size of 8-20: ");
	    	error = scanf("%d", &size);
	    } 
	    // if (size % 2 != 0) {
	    // 	printf("Please try again and enter an even board size.\n");
	    // 	//scanf("%d", &size);
	    // }
	    if (size < 8 || size > 20) {
	    	printf("Please enter a board size between 8-20.\n");
	    	//scanf("%d", &size);
	    }

	    else if (size >= 8 || size < 20) {
	    	check = 1;
	    }

	    else
	    {
            printf("ERROR: number has character that is outside 8-100\n");
            // scanf("%d", &size);
            exit(EXIT_FAILURE);
        }
    }

    //set up board
    BoardType board;
    total_moves = size * size;
    board.moves_left = total_moves;
    printf("\n Total possible moves is: %d\n", total_moves);
    printf("To forfeit your turn, type: 100 100\n");
    printf("To end the game, both players have to type: 500 500\n\n");
    size += 1;
    board.size = size;

    board.grid = malloc(size * size * sizeof(piece *));
    for (int i =0; i < size; i++){
    	board.grid[i] = malloc(size * sizeof(piece *));
    	for (int j = 0; j < size; j++) {

    		if ((i == 0) && (j >= 1)) 
    		{
    		 	board.grid[i][j] = header;
    		}
    		else if ((j == 0) && (i >= 1)) 
    		{
    			board.grid[i][j] = header;
    		}
    		else {
    		board.grid[i][j] = empty;
    		}
    	}
    }

    //set up initial pieces
    int midpoint = (size + 1) / 2;
    board.grid[midpoint][midpoint] =  black;
    board.grid[midpoint][midpoint-1] =  white;
    board.grid[midpoint-1][midpoint-1] =  black;
    board.grid[midpoint-1][midpoint] =  white;

    printBoard(&board);

	//get moves
    while (board.moves_left != 0) {
    	//black's move
    	move(&board, 1);

    	//white's move
    	move(&board, 2);
    }

    return board;
}

void runGame()
{
	int size, move, r_ptr, c_ptr;
	int check = 1;
	int b_total = 0;
	int w_total = 0;
	char error;

	BoardType board = initBoard();

	//count up pieces to find winner
	for (int k = 0; k < board.size; k++) {
    	for (int l = 0; l < board.size; l++) {
    		//printf("Got to inner for loop\n");
    		if (board.grid[k][l] == black) {
    			b_total += 1;
    		}
    		else if (board.grid[k][l] == white) {
    			w_total += 1;
    		}
    	}
    }
    printf("Total black pieces: %d\n", b_total);
    printf("Total white pieces: %d\n", w_total);
    if (b_total > w_total) {
    	printf("Player 1 wins!\n");
    }
    else if (w_total > b_total) {
    	printf("Player 2 wins!\n");
    }
    else {
    	printf("It's a tie?! Both players win!\n");
    }

	cleanBoard(&board);

}
