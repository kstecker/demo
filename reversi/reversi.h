#ifndef REVERSI_H_
#define REVERSI_H_

typedef enum
{
	empty,
	black,
	white,
	header
}piece;

class PlayerType{
private:
	//TODO: make piece private. making public for debugging convenience right now
	//piece color;
public:
	piece color;
	piece getColor();
	void initPlayer(piece);
};

class BoardType{
private:
	int size;
	int r_ptr;
	int c_ptr;
	int checkEmpty();
	int logic(int color);
	int downLogic(int color);
	int upLogic(int color);
	int rightLogic(int color);
	int leftLogic(int color);
	int ld_diagonal(int color);
	int rd_diagonal(int color);
	int lu_diagonal(int color);
	int ru_diagonal(int color);

public:
	piece **grid;
	int moves_left;

	// void setSize(int);
	void move(PlayerType);
	int getSize();
	void getMove(int, int);
	void initBoard();
	void printBoard();
	void cleanBoard();
};

class GameType{
public:
	BoardType board;
	PlayerType player1;
	PlayerType player2;
	void runGame();
};

// Initialize the board with the user-provided input
// BoardType initBoard();

// Run the game
// void runGame();

// void printBoard();

// // Clean up memory (as needed)
// void cleanBoard(BoardType  *board);

#endif /* REVERSI_H_ */